#!/bin/bash
export PRENOM=${1:?"Vous devez indiquer le prénom"}
sudo dnf install -y wget git nc epel-release
sudo dnf -y install htop tmux 
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum repolist -v
sudo dnf -y install docker-ce docker-ce-cli containerd.io
sudo dnf install -y git wget nc tmux
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -G docker ec2-user
sudo groupadd ensiie
sudo useradd -m  -s /bin/bash -G docker -g ensiie $PRENOM
sudo echo "$PRENOM    ALL=(ALL)       NOPASSWD: ALL" >>   /tmp/91-$PRENOM
sudo chmod 440 /tmp/91-$PRENOM
sudo mv  /tmp/91-$PRENOM  /etc/sudoers.d/.
sudo chown root:root  /etc/sudoers.d/91-$PRENOM
sudo mkdir "/home/$PRENOM/.ssh/"
sudo cp -p  /home/ec2-user/.ssh/authorized_keys  /home/$PRENOM/.ssh/authorized_keys
sudo chown -R "$PRENOM:ensiie" /home/$PRENOM/.ssh
sed '43s#h \\W\]\\\\\$ \"#h a_changer \\W\]\\\\\$ \"#' /etc/bashrc | sed "s/a_changer/$PRENOM/" > toto
sudo mv toto /etc/bashrc
sudo chown root:root /etc/bashrc
sudo chown 644 /etc/bashrc
