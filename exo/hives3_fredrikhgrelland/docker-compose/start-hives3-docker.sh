#!/bin/bash
#
#

if [ $(which mc >/dev/null 2>&1 ; echo $?) -eq 1 ]
then  ./install_minio_cli.sh
fi

if [ $(docker image ls | grep 'fredrikhgrelland/hive' | wc -l) -eq 0 ]
then make force-build
fi

if [ $(docker network ls | grep 'hives3' | wc -l) -eq 0 ]
then docker network create -d bridge hives3
fi

if [ ! -f  ~/ensiie/exo/files/labo_hive.csv ]
then gunzip  ~/ensiie/exo/files/labo_hive.csv.gz
fi	


make up
my_ip=`ip route get 1 | awk '{ for (i=1;i<=NF;i++) { if ( $i == "src" ) { print $(i+1) ; exit } } }'`

echo "minio console: http://${my_ip}:9090/"
#echo "trino console: http://[adresse_ip_VM_Eviden]:8085/"

docker compose ps
