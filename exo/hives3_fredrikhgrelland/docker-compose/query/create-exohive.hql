CREATE EXTERNAL TABLE matable ( 
Id INT, 
PostTypeId INT, 
CreationDate BIGINT, 
Score INT, 
ViewCount INT, 
Body STRING, 
OwnerUserId INT, 
LastActivityDate BIGINT, 
Title STRING, 
AnswerCount INT, 
CommentCount INT, 
FavoriteCount INT, 
Tags ARRAY<STRING>) 
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
COLLECTION ITEMS TERMINATED BY ',' 
STORED AS TEXTFILE 
LOCATION 's3a://hive/matable/' ; 
