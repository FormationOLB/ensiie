import_region () {
    export REGION=$1
    FILE=/data/opensearch/bano-data/bano-$REGION.csv
    curl -XDELETE https://os01:9200/bano-$REGION?pretty -u 'admin:admin' -k
    cat $FILE | /usr/share/logstash/bin/logstash -f /data/opensearch/bano-data-opensearch.conf --path.data /tmp
}

DEPTS=95
#for i in {01..19} 2A 2B $(seq 21 $DEPTS) {971..974} {976..976} ; do
#for i in 33 31 75 13 69 44 59 40 06 78 $(seq 91 $DEPTS) ; do
#for i in 33 ; do

# ========================================================================== #
# TP BOTE : mettre vos 3 numeros de département comme les exemples ci-dessus #
# ========================================================================== #
for i in 33 24 40 16 17 64

do
    DEPT=$(printf %02d $i)
    import_region $DEPT
done
