
recup_region () {
    export REGION=$1
    FILE=https://bano.openstreetmap.fr/data/bano-$REGION.csv
    mkdir -p bano-data 2>/dev/null
    cd bano-data
    wget $FILE
    cd -
}

#delete_region () {
#    export REGION=$1
#    curl -XDELETE es01:9200/bano-$REGION?pretty
#}
#DEPTS=95
#for i in {01..19} $(seq 21 $DEPTS) {971..974} {976..976} ; do
#    DEPT=$(printf %02d $i)
#    delete_region $DEPT
#done

DEPTS=95
#for i in {01..19} 2A 2B $(seq 21 $DEPTS) {971..974} {976..976} ; do
#for i in 33 31 75 13 69 44 59 40 06 78 $(seq 91 $DEPTS) 

# ========================================================================== #
# TP BOTE : mettre vos 3 numeros de département comme les exemples ci-dessus #
# ========================================================================== #
#for i in 01 03 07 15 26 38 42 43 63 69 73 74 21 71 39 90 70 25 89 70 71 89 58 10 22 29 35 18 28 36 37 41 45 67 68 88 57 54 55 08 52 51 62 59 80 75 77 78 91 92 93 14 61 50 27 60 76 16 17 19 79 86 87

#for i in {971..974} 976 
for i in 33 24 40 16 17 64
do
    DEPT=$(printf %02d $i)
    recup_region $DEPT
done
#    import_region 2A
#    import_region 2B
