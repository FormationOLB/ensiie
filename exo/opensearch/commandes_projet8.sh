curl -X GET -o espaces3.csv "https://opendata.paris.fr/api/explore/v2.0/catalog/datasets/volumesnonbatisparis/records?limit=100&offset=0&timezone=UTC&include_app_metas=False"
cat espaces3.csv | jq -r '.records[].record.fields' > espaces3.json 
cat espaces3.json | jq -r '[.geom_x_y.lon, .geom_x_y.lat, .c_nat_nb, .l_nat_nb, .c_src, .l_src, .m2, .x, .y, .n_ar, .n_qu, .d_cre, .d_maj, .objectid, .n_sq_nb, .n_sq_qu, .n_sq_ar, .n_sq_pf, .st_area_shape, .st_perimeter_shape] | @csv' > espaces_hive.csv


cd ~/ensiie/exo/files
docker exec -it namenode bash
hdfs dfs -mkdir -p /ensiie/projet8
hdfs dfs -put /data/hdfs/formation/espaces_hive.csv /ensiie/projet8/.
hdfs dfs -ls /ensiie/projet8
exit;


docker exec -it hive-server bash
/opt/hive/bin/beeline -u jdbc:hive2://hive-server:10000
CREATE DATABASE IF NOT EXISTS ensiie;
CTRL-d;
/opt/hive/bin/beeline -u jdbc:hive2://hive-server:10000/ensiie

create TABLE espaces (
geom_x DOUBLE,
geom_y DOUBLE,
c_nat_nb STRING,
l_nat_nb STRING,
c_src STRING,
l_src STRING,
m2 DOUBLE,
x DOUBLE,
y DOUBLE,
n_ar INT,
n_qu DOUBLE,
d_cre STRING,
d_maj STRING,
objectid INT,
id_seq INT,
quartier INT,
arrondissement INT,
parcelle INT,
st_area DOUBLE,
st_perim DOUBLE)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ',' COLLECTION ITEMS TERMINATED BY '\t' STORED AS
TEXTFILE LOCATION '/ensiie/projet8';


SELECT espaces.geom_x, espaces.geom_y, espaces.quartier, espaces.st_area, espaces.d_cre, espaces.objectid from espaces limit 5;

SELECT SUBSTRING(espaces.quartier, -2) as quartier, count(*) from espaces group by quartier;

SELECT quartier, AVG(st_area) AS moyenne_zone, MAX(st_area) AS 
plus_grande_zone, MAX(st_perim) AS plus_grand_perimetre
FROM espaces
GROUP BY quartier
ORDER BY moyenne_zone ASC;

cd ~/ensiie/exo/opensearch
curl -X GET -o volumesnonbatisparis.csv https://opendata.paris.fr/api/explore/v2.1/catalog/datasets/volumesnonbatisparis/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B
docker exec -it logstash bash
cat /data/opensearch/volumesnonbatisparis.csv | /usr/share/logstash/bin/logstash -f /data/opensearch/volumesnonbatisparis-logstash.conf --path.data /tmp

