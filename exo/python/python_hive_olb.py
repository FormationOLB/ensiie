## IMPORTS
from pyspark import SparkConf, SparkContext
from pyspark.sql import Row, SQLContext, HiveContext
import datetime ;
import os

## CONSTANTS

APP_NAME = "Creation table Hive"
conf = SparkConf().setAppName(APP_NAME)
conf = conf.setMaster("local[*]")
sc = SparkContext(conf=conf)

sc.setLogLevel("WARN") ;
sqlContext = HiveContext(sc)
sqlContext.sql("show databases").show() ;
sqlContext.sql("show tables").show() ;
sqlContext.sql("select count(*) from matable").show() ;
sqlContext.sql("SELECT ViewCount, Title, Body FROM matable WHERE PostTypeId = 1 ORDER BY ViewCount DESC LIMIT 10").show() ;
sqlContext.sql("SELECT COUNT(tags_col) as total, tags_col FROM matable LATERAL VIEW explode(tags) tags_table as tags_col WHERE tags_col IS NOT NULL and tags_col <> 'null' GROUP BY tags_col ORDER BY total DESC LIMIT 20").show() ;
