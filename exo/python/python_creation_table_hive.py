settings = [
  ("hive.exec.dynamic.partition", "true"),
  ("hive.exec.dynamic.partition.mode", "nonstrict"),
  ("spark.sql.orc.filterPushdown", "true"),
  ("hive.msck.path.validation", "ignore"),
  ("spark.sql.caseSensitive", "true"),
  ("spark.speculation", "false"),
  ("hive.metastore.authorization.storage.checks", "false"),
  ("hive.metastore.cache.pinobjtypes", "Table,Database,Type,FieldSchema,Order"),
  ("hive.metastore.client.connect.retry.delay", "5s"),
  ("hive.metastore.client.socket.timeout", "1800s"),
  ("hive.metastore.connect.retries", "12"),
  ("hive.metastore.execute.setugi", "false"),
  ("hive.metastore.failure.retries", "12"),
  ("hive.metastore.pre.event.listeners", "org.apache.hadoop.hive.ql.security.authorization.AuthorizationPreEventListener"),
  ("hive.metastore.sasl.enabled", "false"),
  ("hive.metastore.schema.verification", "false"),
  ("hive.metastore.schema.verification.record.version", "false"),
  ("hive.metastore.server.max.threads", "100000"),
  ("hive.metastore.uris", "thrift://hive-metastore:9083"),
  ("hive.metastore.warehouse.dir", "thrift://hive-metastore:9083"),
  ("hive.metastore.authorization.storage.checks", "/user/hive/warehouse")
]

## IMPORTS
from pyspark import SparkConf, SparkContext
from pyspark.sql import Row, SQLContext, HiveContext
import datetime ;
import os

## CONSTANTS 
 
APP_NAME = "Hadoop Data Anaysis - hive - creation table via python"
conf = SparkConf().setAppName(APP_NAME)
conf = conf.setMaster("local[*]")
sc = SparkContext(conf=conf)
sc.setLogLevel("WARN") ;
sqlContext = HiveContext(sc)
sqlContext.sql("use default").show() ;
sqlContext.sql("show tables").show() ;
sqlContext.sql("DROP TABLE IF EXISTS matable2") ;
sqlContext.sql("DROP TABLE IF EXISTS matable3") ;
sqlContext.sql("show tables").show() ;
sqlContext.sql("CREATE EXTERNAL TABLE matable2 (  \
Id INT,  \
PostTypeId INT,  \
CreationDate BIGINT,  \
Score INT,  \
ViewCount INT,  \
Body STRING,  \
OwnerUserId INT,  \
LastActivityDate BIGINT,  \
Title STRING,  \
AnswerCount INT,  \
CommentCount INT,  \
FavoriteCount INT,  \
Tags ARRAY<STRING>)  \
ROW FORMAT DELIMITED  \
FIELDS TERMINATED BY '\t'  \
COLLECTION ITEMS TERMINATED BY ','  \
STORED AS TEXTFILE  \
LOCATION '/user/Olivier/matable2/'") ;
sqlContext.sql("show tables").show() ;
sqlContext.sql("select count(*) from matable2").show() ;
sqlContext.sql("SELECT ViewCount, Title, Body FROM matable2 WHERE PostTypeId = 1 ORDER BY ViewCount DESC LIMIT 10").show() ;
sqlContext.sql("SELECT COUNT(tags_col) as total, tags_col FROM matable2 LATERAL VIEW explode(tags) tags_table as tags_col WHERE tags_col IS NOT NULL and tags_col <> 'null' GROUP BY tags_col ORDER BY total DESC LIMIT 20").show() ;
