#!/bin/bash
#
#

mkdir -p zookeeper/{data,logs} 2>/dev/null
my_ip=`ip route get 1 | awk '{ for (i=1;i<=NF;i++) { if ( $i == "src" ) { print $(i+1) ; exit } } }'`
export EXPOSED_HOSTNAME=${my_ip}
export DOCKER_HOST_IP=${EXPOSED_HOSTNAME}
docker compose up -d --remove-orphans

echo "kafka-manager: http://${my_ip}:9000"
echo "kafka-ui: http://${my_ip}:8081"
