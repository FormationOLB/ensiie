#!/bin/bash
#
#

my_ip=`ip route get 1 | awk '{ for (i=1;i<=NF;i++) { if ( $i == "src" ) { print $(i+1) ; exit } } }'`
export EXPOSED_HOSTNAME=${my_ip}
docker compose down -v
sudo /bin/rm -rf zookeeper/{data,logs}

