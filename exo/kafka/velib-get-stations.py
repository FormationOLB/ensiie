import json
import time
import urllib.request

from kafka import KafkaProducer

API_KEY = "votre cle JC DECAUX" # your own API
url = "https://api.jcdecaux.com/vls/v1/stations?apiKey={}".format(API_KEY)

producer = KafkaProducer(bootstrap_servers="localhost:9092")

#        producer.send("velib-stations", json.dumps(station).encode())
while True:
    response = urllib.request.urlopen(url)
    stations = json.loads(response.read().decode())
    print("Recupered {} station records".format(len(stations)))
    for station in stations:
        producer.send(("velib-stations", json.dumps(station).encode(), key=str(station["number"]).encode())
    print("Produced {} station records".format(len(stations)))
    time.sleep(1)
