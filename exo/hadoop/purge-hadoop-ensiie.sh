#!/bin/bash
#
#

docker compose -f ~/ensiie/exo/hadoop/docker-compose-ensiie-v3.yml down -v
docker volume list | grep hadoop | awk '{ print $2 }' | xargs docker volume rm --force
