#!/bin/bash
#


export HIVE_VERSION=3.1.3
export POSTGRES_LOCAL_PATH=~/ensiie/exo/hadoop/postgresql-42.7.3.jar
export HADOOP_HOME="/opt/hadoop"

docker compose -f ~/ensiie/exo/hadoop/docker-compose-ensiie.yml down -v
docker volume list | grep hadoop | awk '{ print $2 }' | xargs docker volume rm --force

